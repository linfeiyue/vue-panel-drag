// 深拷贝
export const deepcopy = function (source) {
  if (!source) {
    return source
  }
  let sourceCopy = source instanceof Array ? [] : {}
  for (let item in source) {
    sourceCopy[item] = typeof source[item] === 'object' ? deepcopy(source[item]) : source[item]
  }
  return sourceCopy
}

var styleKey = {
  backgroundImage: 'url(%s)'
}

export function createStyles (config) {
  let data = config.data
  let styles = {}
  if (data.style) {
    for (let key in data.style) {
      let val = data.style[key]
      if (val === null) continue
      if (key in styleKey) {
        val = styleKey[key].replace('%s', val)
      }
      styles[key] = val
    }
  }
  return styles
}

// 产生随机数函数
const chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G']
export function RndString (n) {
  var res = ''
  for (var i = 0; i < n; i++) {
    var id = Math.ceil(Math.random() * 17)
    res += chars[id]
  }
  return res
}
export function RndNum (n) {
  var rnd = ''
  for (var i = 0; i < n; i++) { rnd += Math.floor(Math.random() * 10) }
  return rnd
}
