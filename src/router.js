import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Login from './views/login'

Vue.use(Router)

let router = new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/program',
      children: [
        {
          path: '/program',
          name: 'program',
          component: () => import('./views/program')
        }
      ]
    }
  ]
})

export default router
