import _ from 'lodash'
import { EDIT_CONTEXTMENU } from '../mutation-types'

const state = {
  scale: 100,
  contextMenu: {
    show: false,
    axios: {
      x: null,
      y: null
    },
    menulists: []
  }
}

const getters = {
  scale: state => state.scale / 100,
  contextMenuData: state => state.contextMenu
}

const mutations = {
  updateScale (state, scale) {
    state.scale = scale
  },
  [EDIT_CONTEXTMENU] (state, menuData) {
    state.contextMenu = _.cloneDeep(menuData)
  }
}

export default {
  state,
  getters,
  mutations
}
