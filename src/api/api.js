import axios from 'axios'

export const fileUpload = params => {
  return axios({
    url: '/file',
    method: 'post',
    onUploadProgress: function (progressEvent) {
      if (progressEvent.lengthComputable && params.callback !== undefined) {
        params.callback(progressEvent)
      }
    },
    data: params.data,
    headers: {
      'Content-Type': 'multipart/form-data',
      'Content-MD5': params.md5,
      'Content-SHA256': params.sha256
    }
  }).then(res => res.data)
}
