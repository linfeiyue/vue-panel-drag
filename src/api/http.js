import axios from 'axios'
import * as util from '../utils/util'

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.withCredentials = true
axios.defaults.baseURL = 'http://192.168.0.123:9525/demo/api'

if (process.env.NODE_ENV === 'production') {
  const baseURL = window.location.origin
  const pathname = window.location.pathname
  const name = pathname.split('/index.html')[0]
  axios.defaults.baseURL = baseURL + name + '/api'
}

// 错误处理
axios.interceptors.response.use(
  response => {
    return response
  }, util.catchError)

export default axios
